import numpy as np
from scipy.io import loadmat

num_labels = 10
input_layer_size = 400
hidden_layer_size  = 25

print("Loading data...")
data = loadmat('ex4data1.mat')
X = data['X']
y = data['y']
m = len(y)

data1 = loadmat('ex3weights.mat')
Theta1 = data1['Theta1']
Theta2 = data1['Theta2']

Theta1 = np.array(Theta1 , dtype=np.float64)
Theta2 = np.array(Theta2 , dtype=np.float64)
print('x',X.shape)
print('\ny',y.shape)
print('\nt1',Theta1.shape)
print('\nt2',Theta2.shape)
print('\nFeedforward Using Neural Network ...\n')


def sigmoid(z):
    return 1/(1+np.exp(-z))

def sigmoidGradient(z):
    return np.multiply(sigmoid(z), (1 - sigmoid(z)))



def nnCostFunction(Theta1, Theta2, input_layer_size, hidden_layer_size, num_labels, X, y, lamb):
    m = len(y)
    ones = np.ones((m,1))
    X = np.hstack((ones, X))
    J = 0
    Theta1_grad = np.zeros(Theta1.shape)
    Theta2_grad = np.zeros(Theta2.shape)

    delta1 = np.zeros(Theta1.shape)
    delta2 = np.zeros(Theta2.shape)
    #a = zeros(m, size(Theta1,1));
    #hypo = zeros(m, size(Theta2,1));
    a = np.zeros((m ,Theta1.shape[0]))
    hypo = np.zeros((m, Theta2.shape[0]))
    d3 = np.zeros((m, Theta2.shape[0]))
    d2 = np.zeros((m, Theta1.shape[0]))
    #inside the for loop
    #  a(t,:) = sigmoid(X(t,:)*(Theta1'));
  #m1 = size(a, 1);

#a1 = [ones(m1, 1) a];


 #hypo(t,:) = sigmoid(a1(t,:)*(Theta2'));
 #y_matrix = eye(num_labels)(y,:) ;
 #J = J + ((1/m) * (((-y_matrix(t,:))*log(hypo(t,:))') - (1.-y_matrix(t,:))*(log(1.-hypo(t,:))')))
 #d3(t,:) = hypo(t,:) - y_matrix(t,:);
 #ddash2 = Theta2(:,2:end)'*d3(t,:)'.*sigmoidGradient(X(t,:)*(Theta1'))';
 #d2(t,:) = ddash2';
 #delta1 = delta1 + (d2(t,:)')*X(t,:);
 #delta2 = delta2 + (d3(t,:)')*a1(t,:);

    for t in range(m):
        a[t,:] = sigmoid(X[t,:].dot(Theta1.T))
        m1 = a.shape[0]
        a1 = np.hstack((ones, a))
        hypo[t,:] = sigmoid(a1[t,:].dot(Theta2.T))
        y_matrix  = np.eye(num_labels)[y-1,:]
        J = J + ((1/m)*(-y_matrix[t,:].dot((np.log(hypo[t,:])).T) - (1-y_matrix[t,:]).dot((np.log(1-hypo[t,:])).T)))
        d3[t,:] = hypo[t,:] - y_matrix[t,:]
        ddash2 = (Theta2[:,1:].T).dot(d3[t,:].T)*(sigmoidGradient(X[t,:].dot(Theta1.T)).T)
        d2[t,:] = ddash2;
        delta1 = delta1 + (d2[t:t+1,:].T).dot(X[t:t+1,:])
        delta2 = delta2 + (d3[t:t+1,:].T).dot(a1[t:t+1,:])

    #Theta1_grad = (1/m).*delta1;
#Theta2_grad = (1/m).*delta2;

#Theta1_grad(:,2:end) = Theta1_grad(:, 2:end) + ((lamb/m).*Theta1(:,2:end));
#Theta2_grad(:,2:end) = Theta2_grad(:, 2:end) + ((lamb/m).*Theta2(:,2:end));


 #J = J +    (lamb/(2*m))*(sum(sum((Theta1(:,2:end).*Theta1(:,2:end)),2)) + sum(sum((Theta2(:,2:end).*Theta2(:,2:end)),2)));


    Theta1_grad = (1/m)*delta1
    Theta2_grad = (1/m)*delta2
    Theta1_grad[:,1:] = Theta1_grad[:,1:] + ((lamb/m)*Theta1[:,1:])
    Theta2_grad[:,1:] = Theta2_grad[:,1:] + ((lamb/m)*Theta2[:,1:])
    J = J + (lamb/(2*m))*(np.sum(np.sum((Theta1[:,1:]*Theta1[:,1:]),axis=1)) + np.sum(np.sum((Theta2[:,1:]*Theta2[:,1:]),axis=1)))
    return J


lamb = 3
debug_J  = nnCostFunction(Theta1, Theta2 , input_layer_size, hidden_layer_size, num_labels, X, y, lamb)
print('\n\nCost at (fixed) debugging parameters (w/ lambda =',lamb,'):',debug_J,'\n(for lambda = 3, this value should be about 0.576051)\n\n')
