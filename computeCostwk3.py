import numpy as np
from sigmoid import sig
def compute(theta, X, y ):
    m = len(y)
    J = 0
    #grad = zeros(size(theta));
    grad = np.zeros((3,1))
    #for it =1:m
    #hypo=0;
    #hypo=sigmoid(X(it,:)*theta);
    #J=J+ ((-y(it,1)*log(hypo)) -((1-y(it,1))*log(1-hypo)));

    #grad(1,1)= grad(1,1) + (hypo-y(it,1))*X(it,1);
    #grad(2,1)= grad(2,1) + (hypo-y(it,1))*X(it,2);
    #grad(3,1)= grad(3,1) + (hypo-y(it,1))*X(it,3);
    #end
    for it in range(m):
        hyo =0
        hypo = sig(X[it,:].dot(theta))
        J = J + np.dot(-y[it],(np.log(hypo))) - np.dot((1-y[it]),(np.log(1-hypo)))

        grad[0,0]= grad[0,0] + (hypo-y[it]).dot(X[it,0])
        grad[1,0]= grad[1,0] + (hypo-y[it]).dot(X[it,1])
        grad[2,0]= grad[2,0] + (hypo-y[it]).dot(X[it,2])

    J=J/m
    grad=(1/m)*grad
    return(J,grad)
