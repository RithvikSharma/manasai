from scipy.io import loadmat
import numpy as np
import scipy.optimize as opt

print("Loading Data ....")

data = loadmat('ex3data1.mat')
X = data['X']
y = data['y']
m = len(y)

def sigmoid(z):
    return 1/(1+np.exp(-z))

def predict(Theta1, Theta2, X):
    m = len(y)
    ones = np.ones((m,1))
    X = np.hstack((ones, X))
    p = np.zeros((m,1))
    a2 = sigmoid(X.dot(Theta1.T))
    m1 = len(a2)
    a2 = np.hstack((ones , a2))
    p1 = sigmoid(a2.dot(Theta2.T))
    p = np.argmax(p1 , axis=1)
    p = p+1
    return p



data1 = loadmat('ex3weights.mat')
Theta1 = data1['Theta1']
Theta2 = data1['Theta2']

Theta1 = np.array(Theta1 , dtype=np.float64)
Theta2 = np.array(Theta2 , dtype=np.float64)


pred = predict(Theta1, Theta2, X)
print(pred)
print(y.flatten())
print('\nTraining Set Accuracy: ', 100*np.mean((y.flatten()==pred)))
