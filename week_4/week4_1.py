from scipy.io import loadmat
import numpy as np
import scipy.optimize as opt


data = loadmat('ex3data1.mat')
X = data['X']
y = data['y']

m = len(y)
ones = np.ones((m,1))
X = np.hstack((ones, X))
(m,n) = X.shape

def sigmoid(z):
    return 1/(1+np.exp(-z))

def lrCostFunction(theta, X, y, lambd):
    J = 0
    grad = np.zeros((4,1))
    hypo = sigmoid(X.dot(theta))
    #J = (1/m) * (sum((-(y.*log(hypo))) - ((1.-y).*log(1.-hypo))) + (lambda/2)*(sum(theta.*theta)-(theta(1,1)^2)));
    J = (1/m)*(np.sum((-(y*np.log(hypo)))- ((1-y)*np.log(1-hypo))) +(lambd/2)*((np.sum(theta*theta))-(theta[0,0]**2)))
    #grad = ((1/m) .* ((X')*(hypo-y))) + ((lambda/m).*theta);
    #grad(1,1) = grad(1,1) - ((lambda/m).*theta(1,1));
    grad = ((1/m)*((X.T).dot(hypo-y))) +((lambd/m)*theta)
    grad[0,0] = grad[0,0] - ((lambd/m)*theta[0,0])
    grad = grad.flatten()
    grad = grad.reshape(len(grad),1)
    return(J,grad)


print('\nTesting lrCostFunction() with regularization')

theta_t = np.array([[-2], [-1], [1], [2]] , dtype=np.float64)
#X_t = [ones(5,1) reshape(1:15,5,3)/10]
#y_t = ([1;0;1;0;1] >= 0.5);
new_arr = (np.arange(1,16).reshape((3,5)))/10.0
X_t = np.hstack((np.ones((5,1)), new_arr.T))
y_t = np.array([[1], [0], [1], [0] , [1]] , dtype=np.float64)

lambda_t = 3

(J , grad) = lrCostFunction(theta_t, X_t, y_t, lambda_t)


print('\nCost: ', J)
print('\nExpected cost: 2.534819\n')
print('Gradients:\n')
print(grad, '\n')
print('Expected gradients:\n')
print(' 0.146561\n -0.548558\n 0.724722\n 1.398003\n')
