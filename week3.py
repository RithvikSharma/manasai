import numpy as np
import scipy.optimize as opt
import csv
from computeCostwk3 import compute
from sigmoid import sig

print("Extracting data from ex1data1...")
data = open('ex1data1.txt')
with open ('ex2data1.txt', 'r') as f:
    X = [column[:2] for column in csv.reader(f,delimiter=',')]

with open ('ex2data1.txt', 'r') as f1:
    y = [row[2] for row in csv.reader(f1,delimiter=',')]


m=len(y)
X = np.array(X, dtype=np.float64)
y = np.array(y, dtype=np.float64)
X = np.column_stack((np.ones((m,1)), X))
(m,n) = X.shape

#computing cost

initial_theta = [[0],[0],[0]]
initial_theta = np.array(initial_theta, dtype=np.float64)
(cost, grad) = compute(initial_theta, X, y)
print('Cost at initial theta (zeros): \t', cost, '\n')
print('Expected cost (approx): 0.693\n')
print('Gradient at initial theta (zeros): \n')
print(grad, ' \n')
print('Expected gradients (approx):\n -0.1000\n -12.0092\n -11.2628\n')

#prediction and accuracies

X=np.matrix(X)
y=np.matrix(y)
result = opt.fmin_tnc(func=cost, x0=initial_theta, fprime=grad, args=(X, y))
compute(result[0], X, y)



prob = sig(np.array([1, 45, 85]).dot(theta))
print('For a student with scores 45 and 85, we predict an admission probability of \t', prob,'\n')
print('Expected value: 0.775 +/- 0.002\n\n')

p=np.zeros(m,1)
for iter1 in range(m):
    if sig(X[iter1,:].dot(theta))>=0.5:
        p[iter,0]=1

print("Comparing values in p and y respectively: \n")
print(p)
print('\n\n\n')
print(y)
