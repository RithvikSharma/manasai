import csv
import numpy as np

print("Extracting data from ex1data1...")
data = open('ex1data1.txt')
#data = load('ex1data1.txt');
#X = data(:, 1); y = data(:, 2);
#m = length(y);

with open ('ex1data1.txt', 'r') as f:
    X = [column[0] for column in csv.reader(f,delimiter=',')]

with open ('ex1data1.txt', 'r') as f1:
    y = [row[1] for row in csv.reader(f1,delimiter=',')]

m = len(y)

#X = [ones(m, 1), data(:,1)]; % Add a column of ones to x
#theta = zeros(2, 1); % initialize fitting parameters
#iterations = 1500;
#alpha = 0.01;

X = np.array(X, dtype=np.float64)
y = np.array(y, dtype=np.float64)
X = np.column_stack((np.ones((m,1)), X))
theta = np.zeros((2,1))
iterations = 1500
alpha = 0.01

#Compute cost function in Octave
J = 0
summation=0
for it in range(m):
    summation= summation + (np.dot(X[it,:],theta)-y[it])**2
J = (1/(2*m))*summation
print('\nWith theta = [[0],[0]], Cost computed = ', J)
print('\nExpected cost value (approx) 32.07\n')


#Gradient Descent
J_history = np.zeros((iterations, 1))
for iter in range(iterations):
    costd1=0
    costd2=0
    for iter2 in range(m):
        #costd1= costd1+((theta'*(X(iter2,:))'-y(iter2,1))*X(iter2,1));
        #costd2= costd2+((theta'*(X(iter2,:))'-y(iter2,1))*X(iter2,2));
        costd1= costd1+((np.dot(theta.T,(X[iter2,:]).T)-y[iter2]).dot(X[iter2,0]))
        costd2= costd2+((np.dot(theta.T,(X[iter2,:]).T)-y[iter2]).dot(X[iter2,1]))
    #theta(1,1) = theta(1,1) - alpha*(1/m)*costd1;
    #theta(2,1) = theta(2,1) - alpha*(1/m)*costd2;
    theta[0,0] = theta[0,0] - alpha*(1/m)*costd1
    theta[1,0] = theta[1,0] - alpha*(1/m)*costd2


print('Theta found by gradient descent:\n')
print(theta,'\n')
print('Expected theta values (approx)\n')
print(' -3.6303\n  1.1664\n\n');
